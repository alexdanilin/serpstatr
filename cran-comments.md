## Test environments
* Local Windows 11 Home Edition, R 4.4.0
* Docker Linux image on GitLab CI/CD, R 4.1.0
* win-builder (devel and release)

## R CMD check results
There were no ERRORs, WARNINGs and NOTEs. 
